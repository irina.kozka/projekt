# Przydatne polecenia GIT
​
## basics
​
* **git init** - inicjalizuje repozytorium GIT w katalogu
* **git clone {adres repozytorium}** - klonuje repozytorium do katalogu
* **git status** - pokazuje status repozytorium (pokazuje informację o zmodyfikowanych, nowych, usuniętych oraz nie należące do repozytorium plikach)
* **git add {ścierzka do pliku}** - dodaje plik do repozytorium (np. `git add folder/plik.php`)
* **git add -A** - dodaje wszystkie nie należące do repozytorium pliki
* **git config --global color.ui auto** - włącza koloryzowanie wyników w konsoli
* **git config --global core.pager '{nazwa}'** - ustawia program do przeglądania logów (brak w konsoli)
​
## repop
​
* **git fetch -p** - kasuje branche już nie istniejąca na głównym repo
* **git fetch {nazwa remota}** - pobiera listę zmian z innego repozytorium (w tym pokazuje nowe gałęzie)
* **git remote -v** - lista repo
* **git remote remove {repo}** - usuwa wskazane repo
* **git remote set-url {nazwa repo} {url}** - zmienia adres dla podanego repo
* **git remote add {jakaś nazwa} {adres repozytorium}** - dodaje repozytorium innego użytkownika (`git remote add upstream https://github.com/bluetree-service/idylla.git`)
* **git remote -v** lista wszystkich zewnetrznych repozytoriów
* **git remote rm {nazwa dla remota}** - usuwa zewnętrzne repozytorium
* **git pull** - pobiera zmiany z aktualnej gałęzi
* **git pull {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi
* **git pull {nazwa remota} {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi wybranego zewnętrznego repozytorium
* **git pull --all --prune** - ściąga zmiany z repo + kasuje nieużywane branche
* **git pull --tags** - ściąga tagi
* **git pull --rebase** - 
* **git push** - wypycha zmiany na aktualnie wybraną gałąź
* **git push {nazwa gałęzi}** - wypycha zmiany na wskazaną gałąź
* **git push {nazwa remota} {nazwa gałęzi}** - wypycha zmiany na gałąź wskazanego repozytorium
* **git push --tags** - wysyła tagi na repo
* **git revert -- {plik}** - revert pojedyńczego pliku
* **git reset --soft HEAD^** - cofa zmiany bez usuwania dodanych plików
* **git reset --soft {numer commita}** - cofa zmiany bez usuwania dodanych plików do wskazanego commita (`git reset --soft b87dcea`)
* **git reset --hard {numer commita}** - cofa zmiany włącznie z usunięciem plików do wskazanego commita (`git reset --hard b87dced`)
* **git reset --merge ORIG_HEAD** - resetuje zmiany z ostatniego merg-a
* **git checkout -- {plik}** - przywraca oryginalny plik
* **git checkout {commit} -- {plik}** - przywraca stan pliku ze wskazanego commita
* **git checkout -b {nazwa brancha} origin {nazwa brancha}** - tworzy brancha lokalnie i pobiera go (z aktualnymi zmianami) ze zdalnego miejsca
* **git ls-files** - lista plików z ich ścierzkami w repo (-md + zmodyfikowane i usunięte)
* **git rm --cached** - usuwa z pliki z listy do commitów
* **git rm {plik}** - kasuje z repo plik
* **git rm {plik}** - usuwa plik z repo
## commit
​
* **git commit** - tworzy commita z aktualnie zmienionych plików
* **git commit -m "wiadomosc"** - tworzy commmita z podaną w cudzysłowach wiadomością
* **git commit --amend -m "{wiadomość}"** - umożliwia zmianę ostatniego commita
* **git commit --amend -m "dsfsdf"** - modyfikuje komentarz ostatniego commita
* **git commit --date="2017-08-18T13:23:41" -m ""** - comit ze wskazaną datą
* **git commit -n** - pomija git hooks
* **git revert {numer commita}** - tworzy nowego commita z cofnięciem zmian ze wskazanego commita
* **git amend** - zmienia poprzedniego commita
​
## log
​
* **git log** - wyświetla listę commitów (od najnowszego)
* **git log -{numer}** wyświetla podaną liczbę ostatnich commitów
* **git log --oneline** - wyświetla commity w postaci skróconej
* **git log -{numer} --oneline** wyświetla podaną liczbę ostatnich commitów w postaci skróconej
* **git log --graph --decorate --oneline** - pokazuje graficzny obraz zmian
* **git log --author={nazwa użytkownika}** - pokazuje commity danego użytkownika
* **git shortlog** - lista commitów użytkowników
* **git shortlog -s -n** - lista użytkowników repozytorium
* **git log --author={nazwa}** - comity danego autora
* **git llog --after=2016-08-16 --before=2016-08-30** - commity z podanego zakresu (--until starsze od podanej daty, --since z pred podanej daty)
* **git log --name-status** - podaje statuz zmian przy nazwie pliku (add, mod, delete)
* **git log --stat** - + statystyki zmian w plikach (--shortstat bez ++++---)
* **git log {commit1}..{commit} --no-merges** - pokazuje zmiany pomiędzy 2 commitami bez info o mergach
* **git log -- {plik/katalog}** - log dla pojedyńczego pliku lub wszystkich plików z katalogu
* **git log -5 --pretty=tformat: --numstat** - satystyki zmian w 5 commitach
* **git log --no-merges --pretty=format:'%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s' --date=iso
* **git log --pretty=format:'* %s (%an)' -n 10** - pokazuje tylko nazwy commitów
* **git log --pretty=oneline -15 | awk '{print $2}' | sort | uniq | grep -i {ticket} | sed 's/\[\(.*\)\]/\1/g'** - pokaże tylko nazwy ticketów (gdy message zgody z formatem [NAME-111] some message)
* **git log --grep {nazwa}** - szuka commita zawierającego podany tekst
* **git log --author={autor} --name-only** - pokazuje commity wykonane przez autora wraz ze zmodyfikowanymi plikami
* **git log master..develop** - pokazuje różnicę między branchami
​
## merge
​
* **git merge {nazwa gałęzi}** - dołączenie zmian ze wskazanej gałęzi
* **git merge {nazwa remota}/{nazwa gałęzi}** - dołączenie zmian ze wskazanego remota i gałęzi
* **git merge --abort** - przerywa łączenie (możliwe, gdy wystąpią konflikty)
* **git merge --continue** - po rozwiązaniu konflitów zapisuje zmiany
* **git merge --revert** - cofa wszystkie wprowadzone zmiany
​​
